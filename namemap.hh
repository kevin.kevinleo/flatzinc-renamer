#ifndef NAMEMAP_HH
#define NAMEMAP_HH

#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <regex>

struct Location {
  std::string filename = "";
  int sl = 0;
  int sc = 0;
  int el = 0;
  int ec = 0;

  Location();
  Location(const Location& l);
  Location(const std::string& pathHead);
  bool contains(const Location& loc) const;
  bool containsStart(const Location& loc) const;
  void mergeStart(const Location& loc);
  void mergeEnd(const Location& loc);

  std::string toString() const;

  bool operator==(const Location& loc) const;
  bool operator<(const Location& loc) const;
};
namespace std {
  template<> struct hash<Location> {
    size_t operator()(Location const& l) const;
  };
}

struct TElem {
  std::string str;
  bool is_id;
};
struct LocIsEmpty {
    Location loc;
    bool is_final = false;
};

static const std::string empty_string;

class NameMap {
private:
  struct SymbolRecord {
    std::string niceName;
    std::string path;
    Location location;

    SymbolRecord();
    SymbolRecord(const std::string&, const std::string&, const Location&);
  };

public:
  using SymbolTable = std::unordered_map<std::string, SymbolRecord>;
  using ExpressionTable = std::unordered_map<std::string, std::string>;

  using ModelText = std::vector<std::string>;
  using ModelMap = std::unordered_map<std::string, ModelText >;

  NameMap() {}
  NameMap(const std::string& path_filename, bool ee = true, bool aq = false, bool sc = false);

  bool isEmpty() const;
  const std::string& getPath(const std::string& ident) const;
  const Location& getLocation(const std::string& ident) const;
  const Location& getLocation(const int cid) const;
  const std::string& getNiceName(const std::string& ident) const;
  std::string replaceNames(const std::string& text, bool disable_quotes = false);
  std::string getHeatMap(const std::unordered_map<int, int>& con_id_counts, int max_count) const;
  std::unordered_set<Location> getLocations(const std::vector<int>& reasons) const;

  std::string getAssigns(const std::string& path);
  std::string getLastId(const std::string& path);

private:
  NameMap(const SymbolTable& st);

  std::string replaceAssignments(const std::string& path, const std::string& expression);

  void addIdExpressionToMap(const std::string& ident, ModelMap& modelMap);
  void addDecompIdExpressionToMap(const std::string& ident, ModelMap& modelText);

  static std::regex var_name_regex;
  static std::regex assignment_regex;
  SymbolTable _nameMap;
  ExpressionTable _expressionMap;
  bool expand_expressions = false;
  bool add_quotes = false;
  bool show_callsite = false;
  std::unordered_map<std::string, std::vector<TElem> > TPs;
};

#endif // NAMEMAP_HH
