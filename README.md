
#Flatzinc Renamer

Replaces FlatZinc variable identifiers with nicer names and expressions from the original model.
This tool can be useful for debugging MiniZinc decompositions and for interpreting output from a solver (for example nogoods).


Basic usage:

1. Compile with `--output-paths`:
  - `minizinc -c model.mzn --output-paths`
  - This will produce a .paths file alongside the .fzn file

2. The renamer reads text from stdin, replaces any FlatZinc variable names with corresponding 'nice' names or expressions and outputs the text again on stdout. To do this it needs to know where the .paths file is.
  - `cat model.fzn | fznrenamer model.paths`

Some renamed variables may be difficult to interpret for example: 'XI:21|24-21|61:(i=7,i=7,j=1,i=9):domain_encodings.mzn:52|5-52|16'.
This identifier states that the variable was introduced by an expression on line 21, column 24 of the calling .mzn file and was ultimately introduced on line 52, column 5 of the (called) domain_encodings.mzn file in the decomposition library.
The assignments to various index variables along the way are also presented in parentheses.

In some cases you may wish to see the calling expression even if the introduced variable may represent only part of the decomposition of the expression.
For example, the variable introduced to represent the result of an element constraint (x[y] where y is a variable) is introduced inside the standard library and as a result it will be presented with an identifier that looks like: 'XI:...'.
Running fznrenamer with the `--show-decomp-callsite` argument will display the 'x[y]' instead.
